﻿using UnityEngine;
using System.Collections;

public enum GameBlockState
{
	Red = 0, Green, Blue, Yellow, Brown, Purple, Black
}

public class Settings {

	public static readonly int GAME_WIDTH = 4;
	public static readonly int GAME_HEIGHT = 4;
	public static readonly int NUMBER_OF_STARTING_BLOCKS = 2;

	public static readonly Color RED_COLOR = new Color(255, 0, 0);
	public static readonly Color GREEN_COLOR = new Color(0, 255, 0);
	public static readonly Color BLUE_COLOR = new Color(0, 0, 255);
	public static readonly Color YELLOW_COLOR = new Color(255, 255, 102);
	public static readonly Color BROWN_COLOR = new Color(153, 102, 51);
	public static readonly Color PURPLE_COLOR = new Color(153, 0, 204);
	public static readonly Color BLACK_COLOR = new Color(0, 0, 0);
	public static readonly Color WHITE_COLOR = new Color(255, 255, 255);

	public static readonly int BASE_NUMBER = 2;

	public static bool isEven(int num)
	{
		return num % 2 == 0;
	}

}
