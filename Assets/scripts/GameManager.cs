﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public static GameManager SP;

	public static List<List<GameCell>> gameSlots;

	public static Dictionary<GameBlockState, Color> squareColors;
	public static Dictionary<GameBlockState, Color> squareTextColors;

	public Transform GameBlockPrefab;

	// Use this for initialization
	void Start () {
		SP = this;

		// initialize colors
		initializeSquareColors();

		// initialize cell positions
		initialize();

		// initialize gameStarting scene
		initializeStartScene();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp (KeyCode.UpArrow)) {
			Up ();
			refresh();
		} else if (Input.GetKeyUp (KeyCode.DownArrow)) {
			Down();
			refresh ();
		} else if (Input.GetKeyUp (KeyCode.LeftArrow)) {
			Left();
			refresh();
		} else if (Input.GetKeyUp (KeyCode.RightArrow)) {
			Right();
			refresh ();
		}
	}

	private void initialize()
	{
		gameSlots = new List<List<GameCell>>();

		for (int i = 0; i < Settings.GAME_HEIGHT; i++)
		{
			List<GameCell> row = new List<GameCell>();
			for(int j = 0; j < Settings.GAME_WIDTH; j++)
			{
				GameCell gc = GameCell.create(j, i);
				row.Add(gc);
			}

			gameSlots.Add(row);
		}
	}

	private void initializeSquareColors()
	{
		squareColors = new Dictionary<GameBlockState, Color>();
		squareTextColors = new Dictionary<GameBlockState, Color>();

		squareColors[GameBlockState.Red] = Settings.RED_COLOR;
		squareTextColors[GameBlockState.Red] = Settings.BLACK_COLOR;

		squareColors[GameBlockState.Green] = Settings.GREEN_COLOR;
		squareTextColors[GameBlockState.Green] = Settings.BLACK_COLOR;

		squareColors[GameBlockState.Blue] = Settings.BLUE_COLOR;
		squareTextColors[GameBlockState.Blue] = Settings.BLACK_COLOR;

		squareColors[GameBlockState.Yellow] = Settings.YELLOW_COLOR;
		squareTextColors[GameBlockState.Yellow] = Settings.BLACK_COLOR;

		squareColors[GameBlockState.Brown] = Settings.BROWN_COLOR;
		squareTextColors[GameBlockState.Brown] = Settings.BLACK_COLOR;

		squareColors[GameBlockState.Purple] = Settings.PURPLE_COLOR;
		squareTextColors[GameBlockState.Purple] = Settings.BLACK_COLOR;

		squareColors[GameBlockState.Black] = Settings.BLACK_COLOR;
		squareTextColors[GameBlockState.Black] = Settings.WHITE_COLOR;
	}

	private void initializeStartScene()
	{
		int total_slots = Settings.GAME_WIDTH * Settings.GAME_HEIGHT;

		if(Settings.NUMBER_OF_STARTING_BLOCKS > total_slots)
		{
			return;
		}

		if(GameBlockPrefab == null)
		{
			return;
		}

		int currentSquares = 0;

		while(currentSquares < Settings.NUMBER_OF_STARTING_BLOCKS)
		{
			int pos = Random.Range(0, 16);
			int posY = pos / Settings.GAME_WIDTH;
			int posX = pos % Settings.GAME_WIDTH;

			while ((gameSlots[posY][posX] as GameCell).gameObject != null)
			{
				posX++;
				if(posX >= Settings.GAME_WIDTH)
				{
					posY++;
					posX = 0;
					if(posY >= Settings.GAME_HEIGHT)
					{
						posY = 0;
						posX = 0;
					}
				}
			}

			Transform tr = Instantiate(GameBlockPrefab, Vector3.zero, GameBlockPrefab.rotation) as Transform;

			Debug.Log("position x is: " + posX + ", position y is: " + posY);

			(gameSlots[posY][posX] as GameCell).gameObject = tr.gameObject;
			currentSquares++;
		}

		initializeAllSquares();
	}

	private void initializeAllSquares()
	{
		for (int i = 0; i < gameSlots.Count; i++)
		{
			for (int j = 0; j < gameSlots[i].Count; j++)
			{
				GameCell gc = gameSlots[i][j] as GameCell;

				if(gc != null)
				{
					gc.initialize();
				}
			}
		}
	}

	private void Up(){
		for (int r=1; r<Settings.GAME_HEIGHT; r++) {
			for(int c=0; c<Settings.GAME_WIDTH; c++){
				if(gameSlots[r][c].gameObject!=null){
					bool changed = false;
					for(int u=r-1; u>=0; u--){
						if(gameSlots[u][c].gameObject!=null){
							if(gameSlots[u][c].getUpdated()){
								Move (u+1, c, r, c);
							}else{
								if(gameSlots[u][c].gameObject.GetComponent<GameBlockHandler>().state==gameSlots[r][c].gameObject.GetComponent<GameBlockHandler>().state){
									Merge(u, c, r, c);
									//gameSlots[u][c].gameObject.GetComponent<GameBlockHandler>().incre();
									//gameSlots[u][c].setUpdated(true);
									//GameObject.Destroy(gameSlots[r][c].gameObject);
									//gameSlots[r][c].gameObject = null;
								}else{
									Move (u+1, c, r, c);
								}
							}
							changed = true;
							break;
						}
					}
					if(!changed){
						Move (0, c, r, c);
						//gameSlots[0][c].gameObject = gameSlots[r][c].gameObject;
						//gameSlots[r][c].gameObject = null;
					}


				}
			}
		}
		SetUpdate ();

	}

	public void SetUpdate(){
		for (int r=0; r<Settings.GAME_HEIGHT; r++) {
			for(int c=0; c<Settings.GAME_WIDTH; c++){
				gameSlots[r][c].setUpdated(false);
			}
		}
	}

	public void Merge(int nx, int ny, int ox, int oy){
		gameSlots[nx][ny].gameObject.GetComponent<GameBlockHandler>().incre();
		gameSlots[nx][ny].setUpdated(true);
		GameObject.Destroy(gameSlots[ox][oy].gameObject);
		gameSlots[ox][oy].gameObject = null;
	}

	public void Move(int nx, int ny, int ox, int oy){
		gameSlots[nx][ny].gameObject = gameSlots[ox][oy].gameObject;
		gameSlots[ox][oy].gameObject = null;
	}

	public void Down(){
		for (int r= Settings.GAME_HEIGHT-2; r>=0; r--){
			for(int c=0; c<Settings.GAME_WIDTH; c++){
				if(gameSlots[r][c].gameObject!=null){
					bool changed = false;
					for(int d=r+1; d<Settings.GAME_HEIGHT; d++){
						if(gameSlots[d][c].gameObject!=null){
							if(gameSlots[d][c].getUpdated()){
								Move (d-1, c, r, c);
							}else{
								if(gameSlots[d][c].gameObject.GetComponent<GameBlockHandler>().state==gameSlots[r][c].gameObject.GetComponent<GameBlockHandler>().state){
									Merge(d, c, r, c);
								}else{
									Move (d-1, c, r, c);
								}
							}
							changed = true;
							break;
						}
					}
					if(!changed){
						Move (Settings.GAME_HEIGHT-1, c, r, c);
					}
					
					
				}
			}
		}
		SetUpdate ();

	}

	public void Left(){
		for(int c=1; c<Settings.GAME_WIDTH; c++){
			for (int r= 0; r<Settings.GAME_HEIGHT; r++){
				if(gameSlots[r][c].gameObject!=null){
					bool changed = false;
					for(int left=c-1; left>=0; left--){
						if(gameSlots[r][left].gameObject!=null){
							if(gameSlots[r][left].getUpdated()){
								Move (r, left+1, r, c);
							}else{
								if(gameSlots[r][left].gameObject.GetComponent<GameBlockHandler>().state==gameSlots[r][c].gameObject.GetComponent<GameBlockHandler>().state){
									Merge(r, left, r, c);
								}else{
									Move (r, left+1, r, c);
								}
							}
							changed = true;
							break;
						}
					}
					if(!changed){
						Move (r, 0, r, c);
					}
					
					
				}
			}
		}
		SetUpdate ();

	}

	public void Right(){
		for(int c=Settings.GAME_WIDTH-2; c>=0; c--){
			for (int r= 0; r<Settings.GAME_HEIGHT; r++){
				if(gameSlots[r][c].gameObject!=null){
					bool changed = false;
					for(int right=c+1; right<Settings.GAME_WIDTH; right++){
						if(gameSlots[r][right].gameObject!=null){
							if(gameSlots[r][right].getUpdated()){
								Move (r, right-1, r, c);
							}else{
								if(gameSlots[r][right].gameObject.GetComponent<GameBlockHandler>().state==gameSlots[r][c].gameObject.GetComponent<GameBlockHandler>().state){
									Merge(r, right, r, c);
								}else{
									Move (r, right-1, r, c);
								}
							}
							changed = true;
							break;
						}
					}
					if(!changed){
						Move (r, Settings.GAME_WIDTH-1, r, c);
					}
					
					
				}
			}
		}
		SetUpdate ();
	}

	private void refresh()
	{
		List<Vector2> emptySpace = new List<Vector2>();
		for (int i = 0; i < gameSlots.Count; i++) {
			for (int j = 0; j < gameSlots[i].Count; j++) {
				if (gameSlots [i] [j].gameObject == null) {
					emptySpace.Add (new Vector2(i, j));
				}
			}
		}

		if (emptySpace.Count > 0) {
			Debug.Log ("***** create new transform");
			int p = Random.Range(0, emptySpace.Count);
			Transform tr = Instantiate(GameBlockPrefab, Vector3.zero, GameBlockPrefab.rotation) as Transform;
			
			Debug.Log("position x is: " + emptySpace[p].x + ", position y is: " + emptySpace[p].y);
			
			(gameSlots[(int)(emptySpace[p].x)][(int)(emptySpace[p].y)] as GameCell).gameObject = tr.gameObject;
		}

		for (int i = 0; i < gameSlots.Count; i++) {
			for (int j = 0; j < gameSlots[i].Count; j++) {
				GameCell gc = gameSlots [i] [j] as GameCell;
				
				if (gc != null) {
					gc.refresh ();
				}
			}
		}
	}

}
