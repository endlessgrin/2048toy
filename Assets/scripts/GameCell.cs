﻿using UnityEngine;
using System.Collections;

public class GameCell {

	private int pos_x;
	private int pos_y;
	private bool updated;

	private GameObject cellBlock;

	public bool getUpdated()
	{
		return updated;
	}

	public void setUpdated(bool value)
	{
		updated = value;
	}
	public int posX
	{
		get { return pos_x; }
		set { }
	}

	public int posY
	{
		get { return pos_y; }
		set { }
	}

	public GameObject gameObject
	{
		get { return cellBlock; }
		set { cellBlock = value; }
	}

	public static GameCell create(int posX, int posY)
	{
		GameCell gc = new GameCell(posX, posY);

		return gc;
	}

	public GameCell(int posX, int posY)
	{
		pos_x = posX;
		pos_y = posY;
		cellBlock = null;
		updated = false;
	}

	public void initialize()
	{
		refresh ();
	}

	public void refresh()
	{
		if(cellBlock != null)
		{
			Debug.Log("haha");
			float panelScaleX = GameManager.SP.transform.localScale.x;
			float panelScaleY = GameManager.SP.transform.localScale.y;
			
			Debug.Log("panel scale x is: " + panelScaleX + ", panel scale y is: " + panelScaleY);
			
			float cellScaleX = cellBlock.transform.localScale.x;
			float cellScaleY = cellBlock.transform.localScale.y;
			
			Debug.Log ("cell scale x is: " + cellScaleX + ", cell scale y is: " + cellScaleY);
			
			float width = GameManager.SP.transform.GetComponent<BoxCollider2D>().size.x * panelScaleX;
			float height = GameManager.SP.transform.GetComponent<BoxCollider2D>().size.y * panelScaleY;
			
			float cellWidth = width / Settings.GAME_WIDTH;
			float cellHeight = height / Settings.GAME_HEIGHT;
			
			Debug.Log ("cellWidth is: " + cellWidth + ", cellHeight is: " + cellHeight);
			
			int halfWidth = Settings.GAME_WIDTH / 2;
			int halfHeight = Settings.GAME_HEIGHT / 2;
			
			Debug.Log ("halfWidth is: " + halfWidth + ", halfHeight is: " + halfHeight);
			Debug.Log ("posX is: " + pos_x + ", posY is: " + pos_y);
			
			float posX = (pos_x - (Settings.isEven(Settings.GAME_WIDTH) ? (halfWidth - 0.5f) : halfWidth)) * cellWidth;
			float posY = ((Settings.isEven(Settings.GAME_HEIGHT) ? (halfHeight - 0.5f) : halfHeight) - pos_y) * cellHeight;
			
			Debug.Log ("final x is: " + posX + ", final y is: " + posY);
			
			cellBlock.transform.localPosition =
				new Vector3(
					posX,
					posY,
					cellBlock.transform.localPosition.z
					);
			
			cellBlock.GetComponent<GameBlockHandler>().updateSquare();
		}
	}
}
