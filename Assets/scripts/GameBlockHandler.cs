﻿using UnityEngine;
using System.Collections;

public class GameBlockHandler : MonoBehaviour {
	
	public GameBlockState state;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void incre()
	{

	}

	public void updateSquare()
	{
		int pow = ((int) state) + 1;
		float finalNumber = Mathf.Pow(Settings.BASE_NUMBER, pow);

		gameObject.GetComponent<SpriteRenderer>().color = GameManager.squareColors[state];
		gameObject.GetComponentInChildren<TextMesh>().text = finalNumber.ToString();
		gameObject.GetComponentInChildren<TextMesh>().color = GameManager.squareTextColors[state];
	}

}
